// Fonction pour masquer et afficher les divs pour les expériences
function masquer_div(id) {
  var element = document.getElementById(id);
  if (element.style.display === 'block') {
    element.style.display = 'none';
  } else {
    element.style.display = 'block';
  }
}

// Fonction qui vérifie les caractères du mail saisis
function checkEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function validate() {
  var email = document.getElementById("email").value;
  if (checkEmail(email)) {
    return true;
  } else {
    window.alert('Adresse e-mail non valide');
    return false;
  }
}

// Fonction Navbar responsive
function myFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}

// Fonction href #apropos
function hrefApropos(){
  window.location.href = '#apropos';
}

document.addEventListener("DOMContentLoaded", function(){
  var loader = document.getElementById('loader');
  var intro = document.querySelector('.intro');

  // Créez un nouvel élément image pour charger l'image en arrière-plan
  var bgImage = new Image();
  bgImage.src = "img/bg3.jpg"; // Changez cela pour correspondre à votre chemin d'image
  bgImage.onload = function () {
    // Une fois l'image chargée, appliquez-la en arrière-plan et masquez le loader
    intro.style.backgroundImage = 'url(' + bgImage.src + ')';
    loader.style.display = 'none';
    intro.style.display = 'block';
  };

  // Affiche le loader pendant le chargement de l'image
  loader.style.display = 'block';
  intro.style.display = 'none';

  window.addEventListener('scroll', function() {
    if (window.scrollY > 792) {
      document.getElementById('navbar_top').classList.add('fixed-top');
      // add padding top to show content behind navbar
      var navbar_height = document.querySelector('.navbar').offsetHeight;
      document.body.style.paddingTop = navbar_height + 'px';
    } else {
      document.getElementById('navbar_top').classList.remove('fixed-top');
      // remove padding top from body
      document.body.style.paddingTop = '0';
    }
  });

  loadAndDisplayData();
});

async function loadAndDisplayData() {
  try {
      const response = await fetch('data.json');
      if (!response.ok) {
          throw new Error('Erreur de chargement du fichier JSON : ' + response.statusText);
      }
      const data = await response.json();

      const newsContainer = document.getElementById('news-container');

      // Vérifier si les données sont définies et non nulles
      if (data.newsInformatique && data.newsInformatique.articles) {
          displayArticles(data.newsInformatique.articles);
      } else {
          console.warn('Aucune donnée sur les nouvelles informatiques trouvée.');
      }

      function formatDate(dateString) {
          const date = new Date(dateString);
          return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
      }

      function displayArticles(articles) {
          articles.forEach(article => {
              const articleElement = document.createElement('div');
              articleElement.classList.add('article');

              let imageSrc = 'placeholder.png'; 
              if (article.urlToImage) {
                  imageSrc = article.urlToImage;
                  imageSrc = imageSrc.replace('-150x150', '');
              }

              articleElement.innerHTML = `
                  <img src="${imageSrc}" alt="${article.title}" style="color:white; background-color: black; font-size: 16px; align-content: center; text-align: -webkit-center;">
                  <div class="article-content">
                      <h3 style="color: white">${article.title}</h3>
                      <div class="meta">${formatDate(article.publishedAt)}</div>
                      <p style="color: white">${article.description}</p>
                  </div>
                  <a class="button-article" href="${article.url}" target="_blank">Lire l'article</a>
              `;
              newsContainer.appendChild(articleElement);
          });
      }

  } catch (error) {
      console.error('Une erreur est survenue lors du chargement ou de l\'affichage des données :', error);
  }
}

