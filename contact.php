<?php

ini_set('SMTP', 'smtp.mail.ovh.net');
ini_set('smtp_port', '465');
ini_set('sendmail_from', 'me@adrienvaury.fr');

if (isset($_POST['email']) && isset($_POST['sujet']) && isset($_POST['message'])) {
    $expediteur = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);
    if ($expediteur === false) {
        echo "Adresse email invalide";
        exit;
    }

    $destinataire = 'me@adrienvaury.fr';
    $objet = 'Message de mon site web : ' . htmlspecialchars($_POST['sujet']);
    $message = htmlspecialchars($_POST['message']);

    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/plain; charset=UTF-8' . "\r\n";
    $headers .= 'From: ' . $expediteur . "\r\n";
    $headers .= 'Reply-To: ' . $expediteur . "\r\n";
    $headers .= 'X-Mailer: PHP/' . phpversion();

    if (mail($destinataire, $objet, $message, $headers)) {
        header('Location: remerciement.html');
        exit;
    } else {
        echo "Votre message n'a pas pu être envoyé";
    }
} else {
    echo "Tous les champs sont obligatoires.";
}
?>
